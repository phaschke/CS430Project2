#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

#define MAX_COLOR_VAL 255
//Polymorphism in C
//Structs for camera, sphere, plane
typedef struct{
        int type; //0 for camera, 1 for sphere, 2 for plane.
        union{
                struct{
                        double width;
                        double height;
                } camera;
                struct{
                        double color[3];
                        double position[3];
                        double radius;
                } sphere;
                struct{
                        double color[3];
                        double position[3];
                        double normal[3];
                } plane;
        };
} Object;

typedef struct pixelStruct{
        unsigned char r, g, b;
} Pixel;

//Function Prototypes:
int nextChar(FILE* fd);
void expectChar(FILE* fd, int d);
void skipWS(FILE* fd);
char* nextString(FILE* fd);
double nextNumber(FILE* fd);
double* nextVector(FILE* fd);
void readScene(char* fileName, Object** objects);
int raycast(Object** objects, Pixel *pixelBuffer, int width, int height);
void writeHeader(FILE* fdOut, int width, int height);
double* findCamera(Object** objects);
double sphereIntersection(double* Ro, double* Rd, double* position, double radius);
double planeIntersection(double* Ro, double* Rd, double* position, double* normal);
void printPixelBuffer(FILE* fdOut, Pixel *pixelBuffer, int numPixels);
static inline double sqr(double v);
static inline void normalize(double* v);
static inline double dot(double* u, double* v);


//Helper function to square a number.
static inline double sqr(double v){
        return v*v;
}

//Helper function to normalize a ray
static inline void normalize(double* v){
        double len = sqrt(sqr(v[0]) + sqr(v[1]) + sqr(v[2]));
        v[0] /= len;
        v[1] /= len;
        v[2] /= len;
}

static inline double dot(double* u, double* v) {
          return u[0]*v[0] + u[1]*v[1] + u[2]*v[2];
}

//Global variable line stores the current line number for error reporting.
int line = 1;

int main(int c, char** argv) {
        if(c != 5){
                fprintf(stderr, "Error: not enough arguments, expected: raycast width height input.json output.ppm\n");
                exit(1);
        }

        int width = atoi(argv[1]);
        if(width == 0){
                fprintf(stderr, "Error: Width must be greater than 0.\n");
                exit(1);
        }

        int height = atoi(argv[2]);
        if(height == 0){
                fprintf(stderr, "Error: Height must be bigger than 0.\n");
                exit(1);
        }

        int numPixels = width*height;

        //Allocate memory for all objects in the file.
        Object** objects;
        objects = malloc(sizeof(Object*)*128);

        //Allocate memory for pixel information.
        Pixel *pixelBuffer;
        pixelBuffer = (Pixel *)malloc(sizeof(Pixel)*width*height);
        
        //Parse json to get all objects into the struct array.
        readScene(argv[3], objects);

        //Do the raycasting and write the data to the file.
        raycast(objects, pixelBuffer, width, height);

        FILE* fdOut;
        fdOut = fopen(argv[4], "w");
        if(fdOut == NULL){
                fprintf(stderr, "Problem opening %s for writing.\n", argv[4]);
                exit(1);
        }

        //Write the ppm file header.
        writeHeader(fdOut, width, height);

        //Write the pixel buffer.
        printPixelBuffer(fdOut, pixelBuffer, numPixels);

        return 0;
}

int raycast(Object** objects, Pixel *pixelBuffer, int width, int height){

        double* v = findCamera(objects);
        double cameraWidth = v[0];
        double cameraHeight = v[1];

        double pixHeight = cameraHeight/height;
        double pixWidth = cameraWidth/width;
        double cx = 0;
        double cy = 0;
       
        int y = 0;
        int x = 0;
        for(y=0; y<height; y++){
                for(x=0; x<width; x++){
                        double Ro[3] = {0, 0, 0};
                        //Rd normilize(P - Ro)
                        double Rd[3] = {
                                cx - (cameraWidth/2) + pixWidth * (x + 0.5),
                                cy + (cameraHeight/2) - pixHeight * (y + 0.5),
                                1
                        };
                        normalize(Rd);
                
                        int i = 0;
                        double bestT = INFINITY;
                        double bestTColor[3];
                        while (objects[i] != NULL){
                                double t;
                                double objectColor[3];
                                switch(objects[i]->type){
                                case 0:
                                        //Camera case, do nothing.
                                break;
                                case 1:
                                        //sphereIntersection
                                        objectColor[0] = objects[i]->sphere.color[0];
                                        objectColor[1] = objects[i]->sphere.color[1];
                                        objectColor[2] = objects[i]->sphere.color[2];
                                        t = sphereIntersection(Ro, Rd, objects[i]->sphere.position, objects[i]->sphere.radius);
                                break;
                                case 2:
                                        //plane intersection
                                        objectColor[0] = objects[i]->plane.color[0];
                                        objectColor[1] = objects[i]->plane.color[1];
                                        objectColor[2] = objects[i]->plane.color[2];
                                        t = planeIntersection(Ro, Rd, objects[i]->plane.position, objects[i]->plane.normal);
                                break;
                                default:
                                        fprintf(stderr, "Not a valid object type!\n");
                                        exit(1);
                                }
                                //Handle overlaping. Use closer object.
                                if (t > 0 && t < bestT){
                                        //printf("BestT updated: Old bestT: %lf, New bestT: %lf\n", bestT, t);
                                        bestT = t;
                                        bestTColor[0] = objectColor[0];
                                        bestTColor[1] = objectColor[1];
                                        bestTColor[2] = objectColor[2];
                                }
                        i++;

                        } //End while loop
                        //printf("%lf\n", bestT );
                        if(bestT > 0 && bestT != INFINITY){
                                //Print color to pixel struct
                                pixelBuffer->r = (unsigned char)(bestTColor[0]*MAX_COLOR_VAL);
                                pixelBuffer->g = (unsigned char)(bestTColor[1]*MAX_COLOR_VAL);
                                pixelBuffer->b = (unsigned char)(bestTColor[2]*MAX_COLOR_VAL);

                        } else {
                                //Print black ro pixel struct
                                pixelBuffer->r = 0;
                                pixelBuffer->g = 0;
                                pixelBuffer->b = 0;
                        }
                        *pixelBuffer++;
                } //End for on width
        } //End for on height
        return 0;
}

double sphereIntersection(double* Ro, double* Rd, double* position, double radius){
        double a = sqr(Rd[0]) + sqr(Rd[1]) + sqr(Rd[2]);
        double b = 2 * Rd[0] * (Ro[0]-position[0]) + 2 * Rd[1] * (Ro[1]-position[1]) + 2 * Rd[2] * (Ro[2]-position[2]);
        double c = sqr(position[0]) + sqr(position[1]) + sqr(position[2]) + sqr(Ro[0]) + sqr(Ro[1]) + sqr(Ro[2]) +
                -2*(Ro[0]*position[0] + Ro[1]*position[1] + Ro[2]*position[2]) - sqr(radius);

        double det = sqr(b) - 4 * a * c;
        //If det is less than 0 is does not intersect, else it does.
        if(det < 0){
                //Does not intersect
                return -1;
        }

        det = sqrt(det);

        // (-b - sqrt(-4ac))/2a
        double t0 = (-b - det) / (2 * a);
        if (t0 > 0){
                return t0;
        }

        // (-b + sqrt(-4ac))/2a
        double t1 = (-b + det) / (2 * a);
        if(t1 > 0){
                return t1;
        }

        return -1;
}

double planeIntersection(double* Ro, double* Rd, double* position, double* normal){
        
        double Vd = dot(normal, Rd);

        if(fabs(Vd) < 1.0E-10){
                return -1;
        }
        
        double d = -(dot(position, normal));
        double t = -(dot(normal, Ro) + d) / (Vd);
        
        return t;
}

//Function to print the pixel buffer ro the output file.
void printPixelBuffer(FILE* fdOut, Pixel *pixelBuffer, int numPixels){
        int i, pixelCount;

        for(i = 0; i < numPixels; i++){
                fwrite(&pixelBuffer->r, 1, 1, fdOut);
                fwrite(&pixelBuffer->g, 1, 1, fdOut);
                fwrite(&pixelBuffer->b, 1, 1, fdOut);
        
                *pixelBuffer++;
                pixelCount++;
        }
        return;
}

//Function to write the header of the ppm file to the output file.
void writeHeader(FILE* fdOut, int width, int height){
        if(fprintf(fdOut, "%c%d%c", 'P', 6, 10) == 0){
                fprintf(stderr, "Error: writing magic number to header.\n");
                fclose(fdOut);
                exit(1);
        }
        if(fprintf(fdOut, "%d%c", width, 10) == 0){
                fprintf(stderr, "Error: writing width to header.\n");
                fclose(fdOut);
                exit(1);
        }
        if(fprintf(fdOut, "%d%c", height, 10) == 0){
                fprintf(stderr, "Error: writing height to header.\n");
                fclose(fdOut);
                exit(1);
        }
        if(fprintf(fdOut, "%d%c", MAX_COLOR_VAL, 10) == 0){
                fprintf(stderr, "Error: writing max color value to header.\n");
                fclose(fdOut);
                exit(1);
        }

}

//Function to find the camera object and return the width and height.
//If no camera object is found return error.
double* findCamera(Object** objects){
        int i = 0;
        while (objects[i] != NULL){
                if(objects[i]->type == 0){
                        double* v = malloc(sizeof(double)*2);
                        v[0] = objects[i]->camera.width;
                        v[1] = objects[i]->camera.height;
                        return v;
                }
                i++;
        }
        fprintf(stderr, "Error: No camera objects found in input file.\n");
        exit(1);
}

void readScene(char* filename, Object** objects) {
        int c;
        FILE* fdIn = fopen(filename, "r");

        if (fdIn == NULL) {
                fprintf(stderr, "Error: Could not open file \"%s\"\n", filename);
                exit(1);
        }
  
         skipWS(fdIn);
  
        // Find the beginning of the list
        expectChar(fdIn, '[');

        skipWS(fdIn);

        // Find the objects
        int i;
        while (1) {
                c = fgetc(fdIn);


                if (c == '{') {
                        skipWS(fdIn);
            
                        // Parse the object
                        char* key = nextString(fdIn);
                        if (strcmp(key, "type") != 0) {
                                fprintf(stderr, "Error: Expected \"type\" key on line number %d.\n", line);
                                exit(1);
                        }

                        skipWS(fdIn);

                        expectChar(fdIn, ':');

                        skipWS(fdIn);

                        char* value = nextString(fdIn);
                        
                        int type;
                        if (strcmp(value, "camera") == 0) {
                                objects[i] = malloc(sizeof(Object));
                                objects[i]->type = 0;
                                type = 0;
                        } else if (strcmp(value, "sphere") == 0) {
                                objects[i] = malloc(sizeof(Object));
                                objects[i]->type = 1;
                                type = 1;
                        } else if (strcmp(value, "plane") == 0) {
                                objects[i] = malloc(sizeof(Object));
                                objects[i]->type = 2;
                                type = 2;
                        } else {
                                fprintf(stderr, "Error: Unknown type, \"%s\", on line number %d.\n", value, line);
                                exit(1);
                        }

                        skipWS(fdIn);

                        while (1) {
                                // , }
                                c = nextChar(fdIn);
                                if (c == '}') {
                                         // stop parsing this object
                                        break;
                                } else if (c == ',') {
                                        // read another field
                                        skipWS(fdIn);
                                        char* key = nextString(fdIn);
                                        skipWS(fdIn);
                                        expectChar(fdIn, ':');
                                        skipWS(fdIn);
                                        if (strcmp(key, "width") == 0){
                                                objects[i]->camera.width = nextNumber(fdIn);
                                        }else if (strcmp(key, "height") == 0){
                                                objects[i]->camera.height = nextNumber(fdIn);
                                        }else if (strcmp(key, "radius") == 0) {
                                                objects[i]->sphere.radius = nextNumber(fdIn);
                                        }else if (strcmp(key, "color") == 0){
                                                double* value = nextVector(fdIn);
                                                if(type == 1){
                                                        memcpy(objects[i]->sphere.color, value, sizeof(objects[i]->sphere.color));
                                                }
                                                if(type == 2){
                                                        memcpy(objects[i]->plane.color, value, sizeof(objects[i]->plane.color));
                                                }
                                        }else if (strcmp(key, "position") == 0){
                                                double* value = nextVector(fdIn);
                                                if(type == 1){
                                                        memcpy(objects[i]->sphere.position, value, sizeof(objects[i]->sphere.position));
                                                }
                                                if(type == 2){
                                                        memcpy(objects[i]->plane.position, value, sizeof(objects[i]->plane.position));
                                                }
                                        }else if (strcmp(key, "normal") == 0) {
                                                memcpy(objects[i]->plane.normal, nextVector(fdIn), sizeof(objects[i]->plane.normal));
                                        }else{
                                                fprintf(stderr, "Error: Unknown property, \"%s\", on line %d.\n", key, line);
                                                //char* value = nextString(fdIn);
                                        }
                                        skipWS(fdIn);
                                } else {
                                        fprintf(stderr, "Error: Unexpected value on line %d\n", line);
                                exit(1);
                                }
                        } //End inner while
                        skipWS(fdIn);
                        c = nextChar(fdIn);
                        if (c == ',') {
                                // noop
                                skipWS(fdIn);
                        } else if (c == ']') {
                                objects[i+1] = NULL; //Null terminate object array.
                                fclose(fdIn);
                                return;
                        } else {
                                fprintf(stderr, "Error: Expecting ',' or ']' on line %d.\n", line);
                                exit(1);
                        }
                }else if (c == ']') { //end if '{'
                        //Hit end of file.
                        //objects[i] = NULL; //Null terminate object array.
                        if(i == 0){
                                fprintf(stderr, "Worst scene file EVER!, nothing in it!\n");
                                fclose(fdIn);
                                exit(1);
                        }
                        objects[i+1] = NULL; 
                        fclose(fdIn);
                        break;
                }else{
                        fprintf(stderr, "Error: Expecting '}' or ']' character on line %d.\n", line);
                        fclose(fdIn);
                        exit(1);
                }

                i++; //iterate position in object struct array.
        } //end outer while
} //end function



// nextChar() wraps the getc() function and provides error checking and line
// number maintenance
int nextChar(FILE* fdIn) {
         int c = fgetc(fdIn);
        #ifdef DEBUG
        printf("nextChar: '%c'\n", c);
        #endif
        if (c == '\n') {
                line += 1;
        }
        if (c == EOF) {
                fprintf(stderr, "Error: Unexpected end of file on line number %d.\n", line);
        exit(1);
        }
return c;
}

// expectChar() checks that the next character is d.  If it is not it emits
// an error.
void expectChar(FILE* fdIn, int d) {
        int c = nextChar(fdIn);
        if (c == d) return;
        fprintf(stderr, "Error: Expected '%c' on line %d.\n", d, line);
        exit(1);    
}

// skipWS() skips white space in the file.
void skipWS(FILE* fdIn) {
        int c = nextChar(fdIn);
        while (isspace(c)) {
                c = nextChar(fdIn);
        }
        ungetc(c, fdIn);
}

// nextString() gets the next string from the file handle and emits an error
// if a string can not be obtained.
char* nextString(FILE* fdIn) {
  char buffer[129];
  int c = nextChar(fdIn);
  if (c != '"') {
    fprintf(stderr, "Error: Expected string on line %d.\n", line);
    exit(1);
  }  
  c = nextChar(fdIn);
  int i = 0;
  while (c != '"') {
    if (i >= 128) {
      fprintf(stderr, "Error: Strings longer than 128 characters in length are not supported.\n");
      exit(1);      
    }
    if (c == '\\') {
      fprintf(stderr, "Error: Strings with escape codes are not supported.\n");
      exit(1);      
    }
    if (c < 32 || c > 126) {
      fprintf(stderr, "Error: Strings may contain only ascii characters.\n");
      exit(1);
    }
    buffer[i] = c;
    i += 1;
    c = nextChar(fdIn);
  }
  buffer[i] = 0;
  return strdup(buffer);
}

double nextNumber(FILE* fdIn) {
        double value;
        int read = fscanf(fdIn, "%lf", &value);
        if(read == 0){
                fprintf(stderr, "Failed to read number in on line %d.\n", line);
        }
        return value;
}

double* nextVector(FILE* fdIn) {
        double* v = malloc(3*sizeof(double));
        expectChar(fdIn, '[');
        skipWS(fdIn);
        v[0] = nextNumber(fdIn);
        skipWS(fdIn);
        expectChar(fdIn, ',');
        skipWS(fdIn);
        v[1] = nextNumber(fdIn);
        skipWS(fdIn);
        expectChar(fdIn, ',');
        skipWS(fdIn);
        v[2] = nextNumber(fdIn);
        skipWS(fdIn);
        expectChar(fdIn, ']');
        return v;
}

