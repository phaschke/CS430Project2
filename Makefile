all:
	gcc raycast.c -o raycast -lm

clean: 
	-rm -f raycast
	-rm output.ppm
